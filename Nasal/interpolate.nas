##########################################################
#      DE L'HAMAIDE Clément for Douglas DC-3 C47         #
##########################################################

##############################################
################# ANIMATION  #################
################ INTERPOLATE  ################
##############################################

setlistener("/controls/electric/battery-switch", func(v) {
  if(v.getValue()){
    interpolate("/controls/electric/battery-switch-pos", 1, 0.25);
  }else{
    interpolate("/controls/electric/battery-switch-pos", 0, 0.25);
  }
});

setlistener("/controls/engines/engine/starter", func(v) {
  if(v.getValue()){
    interpolate("/controls/engines/engine/starter-pos", 1, 0.1);
  }else{
    interpolate("/controls/engines/engine/starter-pos", 0, 0.25);
  }
});

setlistener("/controls/engines/engine/magnetos", func(v) {
    interpolate("/controls/engines/engine/magnetos-pos", v.getValue(), 0.25);
});

setlistener("/controls/engines/engine/cowl-flaps-cmd", func(v) {
    interpolate("/controls/engines/engine/cowl-flaps-pos", v.getValue(), 0.25);
});

setlistener("/controls/engines/engine[1]/cowl-flaps-cmd", func(v) {
    interpolate("/controls/engines/engine[1]/cowl-flaps-pos", v.getValue(), 0.25);
});

